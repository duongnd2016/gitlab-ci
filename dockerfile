FROM ruby:2.6.3 as build

RUN gem install bundler

COPY ["./Gemfile", "./Gemfile.lock", "./"]

RUN bundle install

WORKDIR /usr/src/app

COPY [".", "./"]

RUN bundle exec jekyll build -d public

FROM nginx:1.13-alpine

COPY --from=build /usr/src/app/public /usr/share/nginx/html